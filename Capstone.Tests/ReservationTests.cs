﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Capstone.DAL;
using Capstone.Models; 

namespace Capstone.Tests
{
    [TestClass]
    public class ReservationTests: ParentTest
    {
        [TestMethod]
        public void Can_create_Reservation_object()
        {
            //Arrange

            //Act

            //Assert
            Assert.IsNotNull(reservationDAO);

        }
        [TestMethod()]
        public void AddReservation_Venue_ID_2_For10_People_For_Peter_Pan()
        {
            //Arrange 
            ReservationDAO reservation = new ReservationDAO(connectionString);
            DateTime testDate = new DateTime(2025, 01, 01);
            // string spaceId, int attendees, DateTime startDate, string reserverName, int requestedDays

            //Act
            reservation.AddReservation("2",10,testDate,"Peter Pan",2);

            //Assert
            Assert.IsNotNull(reservation);
        }
    }
}
