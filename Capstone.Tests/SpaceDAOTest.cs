﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Capstone.DAL;
using Capstone.Models;

namespace Capstone.Tests
{
    [TestClass]
    public class SpaceDAOTest : ParentTest
    {
        [TestMethod]
        public void Can_create_Space_object()
        {
            //Arrange

            //Act

            //Assert
            Assert.IsNotNull(spaceDAO);

        }
        [TestMethod]
        public void GetSpaces_ShouldReturn_AllSpaces_ForVenue_ID_One()
        {
            //Arrange
            SpaceDAO space = new SpaceDAO(connectionString);

            //Act
            List <Space> result = space.GetSpaces("1");

            //Assert
            Assert.AreEqual(7, result.Count);
        }
        [TestMethod]
        public void GetSpaces_ShouldReturn_AllSpaces_ForVenue_ID_Two()
        {
            //Arrange
            SpaceDAO space = new SpaceDAO(connectionString);

            //Act
            List<Space> result = space.GetSpaces("2");

            //Assert
            Assert.AreEqual(4, result.Count);
        }

        [TestMethod]
        public void GetSpaces_ShouldReturn_AllSpaces_ForVenue_ID_Three()
        {
            //Arrange
            SpaceDAO space = new SpaceDAO(connectionString);

            //Act
            List<Space> result = space.GetSpaces("3");

            //Assert
            Assert.AreEqual(4, result.Count);
        }

         [TestMethod]
         public void SearchForAvailableSpacesTest_Space_IS_Available_For_One_Week_Venue_ID_One_50_Guests()
         {
             //Arrange
             SpaceDAO space = new SpaceDAO(connectionString);
            //YYYY,MM,DD
            DateTime startDateTestValue = new DateTime(2021, 01, 01);
            DateTime endDateTestValue = new DateTime(2021, 01, 08);

            //Act
            List<Space> spaces = space.SearchForAvailableSpaces(1, startDateTestValue, endDateTestValue, 50);
            int actualSpacesAvailabe = spaces.Count;
            //Assert
            Assert.IsNotNull(spaces);
            //Returns Top 5 Results 
            Assert.AreEqual(5, actualSpacesAvailabe);
         }
        [TestMethod]
        public void SearchForAvailableSpacesTest_Space_IS_NOT_Available_For_One_Week_Venue_ID_One_500_Guests_Too_Many()
        {
            //Arrange
            SpaceDAO space = new SpaceDAO(connectionString);
            //YYYY,MM,DD
            DateTime startDateTestValue = new DateTime(2021, 01, 01);
            DateTime endDateTestValue = new DateTime(2021, 01, 08);

            //Act
            List<Space> spaces = space.SearchForAvailableSpaces(1, startDateTestValue, endDateTestValue, 500);
            int actualSpacesAvailabe = spaces.Count;
            //Assert
            Assert.IsNotNull(spaces);
            //Returns Top 5 Results 
            Assert.AreEqual(0, actualSpacesAvailabe);
        }

        [TestMethod]
        public void SearchForAvailableSpacesTest_Space_One_Available_For_Venue_ID_Two_50_Guests()
        {
            //Arrange
            SpaceDAO space = new SpaceDAO(connectionString);
            //YYYY,MM,DD
            DateTime startDateTestValue = new DateTime(2021, 02, 18);
            DateTime endDateTestValue = new DateTime(2021, 02, 19);

            //Act
            List<Space> spaces = space.SearchForAvailableSpaces(2, startDateTestValue, endDateTestValue, 50);
            int actualSpacesAvailabe = spaces.Count;
            //Assert
            Assert.IsNotNull(spaces);
            //Returns Top 5 Results 
            Assert.AreEqual(1, actualSpacesAvailabe);
        }

    }


}
