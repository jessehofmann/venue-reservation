using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.DAL;
using Capstone.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Capstone.Tests
{
    [TestClass]
    public class VenueDAOTest : ParentTest
    {
        [TestMethod]
        public void Can_create_Venue_object()
        {
            //Arrange

            //Act

            //Assert
            Assert.IsNotNull(venueDAO);

        }
        [TestMethod]
        public void GetVenues_Should_Return_AllVenues()
        {
            //Arrange
            VenueDAO venue = new VenueDAO(connectionString);

            //Act
            List<Venue> venues = venue.GetVenues();

            //Assert
            Assert.AreEqual(15, venues.Count);
        }
    }
}
