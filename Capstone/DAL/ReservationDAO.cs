﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Capstone.DAL
{
    public class ReservationDAO
    {
        private string connectionString;

        private string sqlAddReservation = "INSERT INTO reservation (space_id, number_of_attendees, start_date, end_date, reserved_for)" +
                                           "VALUES (@space_id, @number_of_attendees, @start_date, @end_date, @reserved_for)";
        private string sqlNewestReservation = "SELECT r.reservation_id, r.number_of_attendees, r.start_date, r.end_date, r.reserved_for, v.name venue_name, s.name space_name, s.daily_rate daily_rate FROM reservation r " +
                                              "JOIN space s ON  s.id = r.space_id " +
                                              "JOIN venue v ON v.id = s.venue_id " +
                                              "WHERE reservation_id = @newestReservation";

        public ReservationDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }


        public Reservation AddReservation(string spaceId, int attendees, DateTime startDate, string reserverName, int requestedDays)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                int id = int.Parse(spaceId);
                DateTime endDate = startDate.AddDays(requestedDays);

                SqlCommand cmd = new SqlCommand(sqlAddReservation, conn);
                cmd.Parameters.AddWithValue("@space_id", id);
                cmd.Parameters.AddWithValue("@number_of_attendees", attendees);
                cmd.Parameters.AddWithValue("@start_date", startDate);
                cmd.Parameters.AddWithValue("@end_date", endDate);
                cmd.Parameters.AddWithValue("@reserved_for", reserverName);

                cmd.ExecuteNonQuery();

                cmd = new SqlCommand("SELECT MAX(reservation_id) FROM reservation", conn);
                int newestReservation = Convert.ToInt32(cmd.ExecuteScalar());

                cmd = new SqlCommand(sqlNewestReservation, conn);
                cmd.Parameters.AddWithValue("@newestReservation", newestReservation);

                SqlDataReader reader = cmd.ExecuteReader();

                Reservation reservation = new Reservation();
                while (reader.Read())
                {
                    reservation.reservation_id = Convert.ToInt32(reader["reservation_id"]);
                    reservation.number_of_attendees = Convert.ToInt32(reader["number_of_attendees"]);
                    reservation.start_date = Convert.ToDateTime(reader["start_date"]);
                    reservation.end_date = Convert.ToDateTime(reader["end_date"]);
                    reservation.reserved_for = Convert.ToString(reader["reserved_for"]);
                    reservation.venue_name = Convert.ToString(reader["venue_name"]);
                    reservation.space_name = Convert.ToString(reader["space_name"]);
                    reservation.total = Convert.ToInt32(reader["daily_rate"]) * requestedDays; 
                }
                return reservation;
            }
        }
    }
}
