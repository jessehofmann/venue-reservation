﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class SpaceDAO
    {
        private string connectionString;
        private string sqlGetSpaces = "SELECT * FROM space WHERE venue_id = @userInput";
        private string sqlGetAvailableSpaces = "SELECT DISTINCT TOP 5 s.id, s.name,s.daily_rate,s.max_occupancy,s.is_accessible " +
                                                "FROM space s " +
                                                "LEFT JOIN reservation r ON r.space_id = s.id " +
                                                "WHERE s.venue_id = @venue_id AND s.id NOT IN " +
                                                "(SELECT s.id FROM space s " +

                                                "JOIN reservation r ON r.space_id = s.id " +

                                                "WHERE r.end_date= @end_date AND r.start_date= @start_date) " +
                                                "AND @max_occupancy<max_occupancy " +
                                                "GROUP BY  s.id, s.name, s.daily_rate, s.max_occupancy, s.is_accessible;  ";


        public SpaceDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public List<Space> GetSpaces(string userInput)
        {
            List<Space> spaces = new List<Space>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sqlGetSpaces, conn);

                cmd.Parameters.AddWithValue("@userInput", userInput);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Space space = CovenvertReaderToSpace(reader);
                    spaces.Add(space);
                }
            }

            return spaces;
        }
        public Space CovenvertReaderToSpace(SqlDataReader reader)
        {
            Space space = new Space();

            space.id = Convert.ToInt32(reader["id"]);
            space.name = Convert.ToString(reader["name"]);
            if (reader["open_from"] != DBNull.Value)
            {
                space.open_from = Convert.ToInt32(reader["open_from"]);
            }
            if (reader["open_from"] != DBNull.Value)
            {
                space.open_to = Convert.ToInt32(reader["open_to"]);
            }
            space.daily_rate = Convert.ToDecimal(reader["daily_rate"]);
            space.max_occupancy = Convert.ToInt32(reader["max_occupancy"]);
            space.start_month_name = ConvertToMonth(space.open_from);
            space.end_month_name = ConvertToMonth(space.open_to);

            return space;
        }
        public string ConvertToMonth(int month)
        {
            string monthName = "";
            if (month == 1)
            {
                monthName = "Jan.";
            }
            else if (month == 2)
            {
                monthName = "Feb.";
            }
            else if (month == 3)
            {
                monthName = "Mar.";
            }
            else if (month == 4)
            {
                monthName = "Apr.";
            }
            else if (month == 5)
            {
                monthName = "May";
            }
            else if (month == 6)
            {
                monthName = "Jun.";
            }
            else if (month == 7)
            {
                monthName = "Jul.";
            }
            else if (month == 8)
            {
                monthName = "Aug.";
            }
            else if (month == 9)
            {
                monthName = "Sep.";
            }
            else if (month == 10)
            {
                monthName = "Oct.";
            }
            else if (month == 11)
            {
                monthName = "Nov.";
            }
            else if (month == 12)
            {
                monthName = "Dec.";
            }
            return monthName;
        }


        public List<Space> SearchForAvailableSpaces(int venue_id, DateTime start_date, DateTime end_date, int max_occupancy)
        {
            Console.WriteLine($"This is the venue_id: {venue_id}");
            List<Space> availableSpaces = new List<Space>();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sqlGetAvailableSpaces, connection))
                    {
                        command.Parameters.AddWithValue("@venue_id", venue_id);
                        command.Parameters.AddWithValue("@start_date", start_date);
                        command.Parameters.AddWithValue("@end_date", end_date);
                        command.Parameters.AddWithValue("@max_occupancy", max_occupancy);

                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Space space = new Space();
                            space.id = Convert.ToInt32(reader["id"]);
                            space.name = Convert.ToString(reader["name"]);
                            space.daily_rate = Convert.ToDecimal(reader["daily_rate"]);
                            space.max_occupancy = Convert.ToInt32(reader["max_occupancy"]);

                            int isAcccessible = Convert.ToInt32(reader["is_accessible"]);
                            if (isAcccessible == 1)
                            {
                                space.is_accessible = true;
                            }
                            else
                            {
                                space.is_accessible = false;
                            }
                            availableSpaces.Add(space);
                        }

                    }

                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error retrieving spaces.");
                Console.WriteLine(ex.Message);
                throw;
            }
            return availableSpaces;
        }

    }
}
