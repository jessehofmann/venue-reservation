﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class VenueDAO
    {
        // NOTE: No Console.ReadLine or Console.WriteLine in this class


        private string connectionString;
        private string sqlGetVenues = "SELECT venue.id, venue.name venue_name, city.name city_name, city.state_abbreviation, venue.description FROM venue " +
                                      "JOIN city ON city.id = venue.city_id " +
                                      "ORDER BY venue.name";
        private string sqlAddCategories = "SELECT venue.name venue_name, category.name categoryname FROM venue " +
                                          "JOIN category_venue ON category_venue.venue_id = venue.id " +
                                          "JOIN category ON category.id = category_venue.category_id";

        public VenueDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Venue> GetVenues()
        {
            List<Venue> venues = new List<Venue>();
            List<string> venueTitles = new List<string>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sqlGetVenues, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Venue venue = ConvertReaderToVenue(reader);
                    venues.Add(venue);
                }

            }
                venues = AddCategories(venues);

            return venues;
        }

        private Venue ConvertReaderToVenue(SqlDataReader reader)
        {
            Venue venue = new Venue();

            venue.id = Convert.ToString(reader["id"]);
            venue.name = Convert.ToString(reader["venue_name"]);
            venue.description = Convert.ToString(reader["description"]);
            venue.city = Convert.ToString(reader["city_name"]);
            venue.state = Convert.ToString(reader["state_abbreviation"]);

            //venue.category = Convert.ToString(reader["category_name"]);

            return venue;
        }

        private List<Venue> AddCategories(List<Venue> venuesList)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sqlAddCategories, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    foreach(Venue venue in venuesList)
                    {
                        if(venue.name == Convert.ToString(reader["venue_name"]))
                        {
                            venue.category += (Convert.ToString(reader["categoryname"]));
                            venue.category += ",";
                        }
                    }
                }
            }
            return venuesList;
        }
    }
}
