﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Models
{
    public class Venue
    {
        public string id { get; set; }
        public string name { get; set; }
        public int city_id { get; set; }
        public string description { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string category { get; set; }

    }
}
