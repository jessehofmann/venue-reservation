﻿using Capstone.DAL;
using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone
{
    public class UserInterface
    {
        //ALL Console.ReadLine and WriteLine in this class
        //NONE in any other class

        private string connectionString;
        private VenueDAO venueDAO;
        private SpaceDAO spaceDAO;
        private ReservationDAO reservationDAO;

        public UserInterface(string connectionString)
        {
            this.connectionString = connectionString;
            venueDAO = new VenueDAO(connectionString);
            spaceDAO = new SpaceDAO(connectionString);
            reservationDAO = new ReservationDAO(connectionString);
        }

        public void Run()
        {
            bool done = false;
            while (!done)
            {
                string userResponse = DisplayMenu();
                switch (userResponse)
                {
                    case ("1"):
                        ListVenues();
                        break;
                    case ("Q"):
                    case ("q"):
                        done = true;
                        continue;
                    default:
                        Console.WriteLine("Please make a valid selection.");
                        userResponse = Console.ReadLine();
                        break;
                }

            }
        }

        public string DisplayMenu()
        {
            Console.WriteLine("What you like to do?");
            string one = "1) List Venues";
            string quit = "Q) Quit";
            Console.WriteLine(one.PadLeft(one.Length + 5));
            Console.WriteLine(quit.PadLeft(quit.Length + 5));
            return Console.ReadLine();
        }

        public void ListVenues()
        {
            Console.WriteLine("Which venue would you like to view?");
            List<Venue> venues = venueDAO.GetVenues();
            foreach (Venue venue in venues)
            {
                string display = $"{venue.id}) {venue.name}";
                Console.WriteLine(display.PadLeft(display.Length + 5));
            }
            string previous = "R) Return to Previous Screen";
            Console.WriteLine(previous.PadLeft(previous.Length + 5));
            string userInput = Console.ReadLine();

            foreach (Venue venue in venues)
            {
                if (userInput == venue.id)
                {
                    Console.WriteLine($"This is the venue being selected: {userInput}");
                    VenueMenuNavigation(venue);
                }
            }
        }

        public void VenueMenuNavigation(Venue venue)
        {
            Console.WriteLine($"{venue.name}");
            Console.WriteLine($"Location: {venue.city} {venue.state}");
            Console.WriteLine($"Categories: {venue.category}".Trim(','));
            Console.WriteLine();
            Console.WriteLine($"{venue.description}");
            Console.WriteLine();

            string one = "1) View Spaces";
            string two = "2) Search for Reservation";
            string previous = "R) Return to Previous Screen ";
            Console.WriteLine(one.PadLeft(one.Length + 5));
            Console.WriteLine(two.PadLeft(two.Length + 5));
            Console.WriteLine(previous.PadLeft(previous.Length + 5));
            string userInput = Console.ReadLine();
            switch (userInput.ToLower())
            {
                case ("1"):
                    ViewSpace(venue.id);
                    break;
                case ("2"):
                    Console.WriteLine();
                    GatherReservationDetails(int.Parse(venue.id));
                    break;
                case ("R"):
                    break;
                default:
                    Console.WriteLine("Please make a valid selection: ");
                    userInput = Console.ReadLine();
                    break;
            }
        }

        public void ViewSpace(string userInput)
        {
            List<Space> spaces = spaceDAO.GetSpaces(userInput);

            Console.WriteLine();
            Console.Write("".PadRight(5));
            Console.Write("Name".PadRight(30));
            Console.Write("Open".PadRight(7));
            Console.Write("Close".PadRight(7));
            Console.Write("Daily Rate".PadRight(15));
            Console.Write("Max. Occupancy");

            foreach (Space space in spaces)
            {
                Console.WriteLine();
                Console.Write($"#{space.id}".PadRight(5));
                Console.Write($"{space.name}".PadRight(30));
                Console.Write($"{space.start_month_name}".PadRight(7));
                Console.Write($"{space.end_month_name}".PadRight(7));
                Console.Write($"{space.daily_rate:c}".PadRight(15));
                Console.Write($"{space.max_occupancy}");
            }

            Console.WriteLine("\n");
            Console.WriteLine("What would you like to do next?");
            string one = "1) Reserve a Space";
            string previous = "R) Return to Previous Menu";
            Console.WriteLine(one.PadLeft(one.Length + 5));
            Console.WriteLine(previous.PadLeft(previous.Length + 5));
            string input = Console.ReadLine();
            switch (input.ToLower())
            {
                case ("1"):
                    GatherReservationDetails(int.Parse(userInput));
                    break;
                case ("r"):
                    break;
                default:
                    Console.WriteLine("Please make a valid selection");
                    input = Console.ReadLine();
                    break;
            }
        }
        public void GatherReservationDetails(int venue_id)
        {
            Console.WriteLine($"this is the venue_id: {venue_id}");
            Console.Write("When do you need the space [YYYY-MM-DD]: ");
            string dateSpaceNeeded = Console.ReadLine();
            DateTime startDate = DateTime.Parse(dateSpaceNeeded);

            Console.Write("how many days will you need the space? ");
            string daysNedded = Console.ReadLine(); 
            int numberOfDaysRequested = int.Parse(daysNedded);

            DateTime endDate = startDate.AddDays(numberOfDaysRequested);

            Console.Write("How many people will be in attendance? ");
            string peopleNeeded = Console.ReadLine();
            int attendance = int.Parse(peopleNeeded);

            SpaceDAO space = new SpaceDAO(connectionString);

            List<Space> spaces = space.SearchForAvailableSpaces(venue_id, startDate, endDate, attendance);

            DisplaySearchResults(spaces, numberOfDaysRequested);
            ReserveSpace(attendance, startDate, numberOfDaysRequested);
        }

        public void DisplaySearchResults(List<Space> spaces, int numberOfDaysRequested)
        {
            if (spaces.Count == 0)
            {
                Console.WriteLine("There are no spaces avaialable. Please make another selection.");
            }
            else
            {
                Console.WriteLine("The following spaces are available based on your needs: ");
                Console.WriteLine("");
                Console.Write("Space".PadRight(10));
                Console.Write("Name".PadRight(30));
                Console.Write("Daily Rate".PadRight(12));
                Console.Write("Max. Occup.".PadRight(15));
                Console.Write("Accessible".PadRight(15));
                Console.Write("Total Cost");

                foreach (Space space in spaces)
                {
                    Console.WriteLine();
                    Console.Write($"{space.id}".PadRight(10));
                    Console.Write($"{space.name}".PadRight(30));
                    Console.Write($"{space.daily_rate}".PadRight(12));
                    Console.Write($"{space.max_occupancy}".PadRight(15));
                    Console.Write($"{space.is_accessible}".PadRight(15));
                    Console.Write($"{space.daily_rate * numberOfDaysRequested}");
                }
            }
        }
        public void ReserveSpace(int attendance, DateTime requestedDate, int numberOfDaysRequested)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.Write("Which space would you like to reserve (enter 0 to cancel)?");
            string reservationSpaceRequest = Console.ReadLine();
            if(reservationSpaceRequest == "0")
            {
                return;
            }
            Console.Write("Who is the reservation for?");
            string reserverName = Console.ReadLine();
            Reservation reservation = reservationDAO.AddReservation(reservationSpaceRequest, attendance, requestedDate, reserverName, numberOfDaysRequested);

            Console.WriteLine("Thanks for submitting your reservation! The details for your event are listed below:");
            Console.WriteLine();
            Console.WriteLine($"Confirmation #: {reservation.reservation_id}");
            Console.WriteLine($"Venue: {reservation.venue_name}");
            Console.WriteLine($"Space: {reservation.space_name}");
            Console.WriteLine($"Reserved for: {reservation.reserved_for}");
            Console.WriteLine($"Attendees: {reservation.number_of_attendees}");
            Console.WriteLine($"Arrival Date: {reservation.start_date}");
            Console.WriteLine($"Depart Date: {reservation.end_date}");
            Console.WriteLine($"Total Cost: {reservation.total:c}");
            Console.WriteLine();
        }
    }
}

